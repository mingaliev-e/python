FROM centos:7

RUN yum install python3.7 python3.7-pip -y
RUN pip3.7 install -r requirements.txt

RUN mkdir python_api
COPY python-api.py /python_api

CMD ["python3.7", "python_api/python-api.py"]